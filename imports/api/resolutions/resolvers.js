import Resolutions from './resolutions';
import Goals from '../goals/goals';

export default {
  Query: {
    resolutions (obj, args, { userId }) {
	  console.log('args: ',args);
      return Resolutions.find({
        userId
      }).fetch();
    }
  },
  Resolution: {	  /* console.log('resolutionId: ', resolution._id); /* Now we have access to our goals through the resolutions schema itself even though resolutions is not even calling goals */
    goals: resolution =>
      Goals.find({
        resolutionId: resolution._id
      }).fetch(),

    completed: resolution => {
      const goals = Goals.find({
        resolutionId: resolution._id
      }).fetch();
      if (goals.length === 0) return false;
      const completedGoals = goals.filter(goal => goal.completed);
      return goals.length === completedGoals.length;
    }
  },

  Mutation: {
    createResolution (obj, { name }, { userId }) {
	  if (userId) {
		const resolutionId = Resolutions.insert({
          name,
          userId
		});
		return Resolutions.findOne(resolutionId);
	  }
	  throw new Error("Unauthorized");
    }
  }
};
