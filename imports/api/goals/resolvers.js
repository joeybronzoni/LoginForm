import Goals from './goals';

export default {
  Mutation: {
    createGoal(obj, { name, resolutionId }, { userId }) {
	  if (userId) {
		const goalId = Goals.insert({
          name,
          resolutionId,
          completed: false
		});
		return Goals.findOne(goalId);
	  }
	  throw new Error("Unauthorized");
    },
    toggleGoal(obj, { _id }) {
	  const goal = Goals.findOne(_id);
	  goal.completed;
	  Goals.update(_id, {
		$set: {
		  completed: !goal.completed
		}/* The args we pass to set are the only things that are going to get updated and if we tried to run this without $set() then it would override the other properties*/	  /*The things we pass to set are the only things that are going to get updated and if we tried to run this without $set() then it would override the other properties*/
	  });
	  return Goals.findOne(_id);
	}
  }
};
