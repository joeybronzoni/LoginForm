import React, { Component } from "react";
import gql from "graphql-tag";
import { graphql } from "react-apollo";

const createResolution = gql`
  mutation createResolution($name: String!) {
    createResolution(name: $name) {
      _id
    }
  }
`;
console.log('createResolution is = ',createResolution);
class ResolutionForm extends Component {
  state = {
	error: null
  }

  submitForm = () => {
    this.props
      .createResolution({
        variables: {
          name: this.name.value
        }
      })/*// we don't need to use a .then() here because graphql automatically grabs the data
	  .then((data) => {
		// This will give us a promise
		// alert(JSON.stringify(data));
	  })*/
      .catch(error => {
        console.log(error);
		this.setState({error: error.message})
      });
  };

  render() {
    return (
      <div>
		{this.state.error && <p style={{color:'red'}}>{this.state.error}</p>}
        <input type="text" ref={input => (this.name = input)} />
        <button onClick={this.submitForm}>Submit</button>
      </div>
    );
  }
}

/* options is going to be an obj and we are passing in an array of
// this says automatically refetch the data *note look at the .then in ResolutionForm*
   and when
*/
export default graphql(createResolution, {
  name: "createResolution",
  options: {
    refetchQueries: ["Resolutions"]
  }
})(ResolutionForm);
