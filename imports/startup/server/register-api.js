import { createApolloServer } from 'meteor/apollo';
import { makeExecutableSchema } from 'graphql-tools';
import merge from 'lodash/merge';


import ResolutionsSchema from '../../api/resolutions/Resolutions.graphql';
import ResolutionsResolvers from '../../api/resolutions/resolvers';
import UsersSchema from '../../api/users/User.graphql';
import UsersResolvers from '../../api/users/resolvers';
import GoalsSchema from '../../api/goals/Goal.graphql';
import GoalsResolvers from '../../api/goals/resolvers';
// !*!*!*!* Note- to user the graphql tools go to localhost:3000/graphigl //

const typeDefs = [
  ResolutionsSchema,
  UsersSchema,
  GoalsSchema
];

const resolvers = merge(
  ResolutionsResolvers,
  UsersResolvers,
  GoalsResolvers
);

const schema = makeExecutableSchema({
  typeDefs,
  resolvers
});

/* we pass our schema(resolver/typeDefs to our apollo server) */
createApolloServer({ schema });
